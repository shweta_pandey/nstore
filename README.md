# N-Store 

## Storage architectures

Evaluation of different storage architectures in database systems designed for non-volatile memory (NVM).


## Dependencies

- **g++ 4.7+** 
- **autoconf** (`apt-get install autoconf libtool`)
NOTE: brew install autoconf automake libtool  (On MAC)

## Setup
        
```
    ./bootstrap
    ./configure
    make
```

## Test

```
./src/n-store -h 
```

## Changes on local machine
$ sudo mkdir /pmem/n-store
$ sudo ./src/n-store -k 100
$ sudo ./src/n-store -y
$ sudo ./src/n-store -d
