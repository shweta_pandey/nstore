#!/bin/bash
NKEYS=2000000
NRUNS=5

echo "128B alignment running for basic config" >> results/results_wal_ssd.txt
echo "128B alignment running for basic config" >> results/results_lsm_ssd.txt
echo "128B alignment running for basic config" >> results/results_cow_ssd.txt

for ((run = 1; run <= NRUNS; run = run + 1)); do
    echo "Cleaning up from last runs"
    rm /dev/shm/zfile
    rm -rf ../data/n-store-orig/*
    sleep 10
    ./src/nstore -k $NKEYS -a >> results/results_wal_ssd.txt
    sleep 10
    rm /dev/shm/zfile
    rm -rf ../data/n-store-orig/*
    ./src/nstore -k $NKEYS -m >> results/results_lsm_ssd.txt
    sleep 10
    rm /dev/shm/zfile
    rm -rf ../data/n-store-orig/*
    ./src/nstore -k $NKEYS -s >> results/results_cow_ssd.txt
    sleep 10
done

