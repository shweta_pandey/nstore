#!/bin/bash
NKEYS=2000000

#for ((num_txns = 1; num_txns <= 5; num_txns = num_txns + 1)); do
    for ((num_executors = 1; num_executors <= 16; num_executors = num_executors + 1)); do
        echo "Cleaning up from last runs"
        rm -rf /pmem/n-store
        mkdir /pmem/n-store
        rm /dev/shm/zfile
        sleep 10

       for percent_writes in $(seq 0.5 0.25 1)
        do
            echo -e "Num txns: $num_txns\tnum_executors: $num_executors\tpercent_writes: $percent_writes" >> results.txt
            ./src/nstore -k $NKEYS -p $percent_writes -e $num_executors >> results.txt
            sleep 10
            echo -e "For YSCB: Num txns: $num_txns\tnum_executors: $num_executors\tpercent_writes: $percent_writes" >> results.txt
            ./src/nstore -y -k $NKEYS -p $percent_writes -e $num_executors >> results.txt
            sleep 10
            echo -e "For YSCB with optimized WAL: Num txns: $num_txns\tnum_executors: $num_executors\tpercent_writes: $percent_writes" >> results.txt
            ./src/nstore -a -y -k $NKEYS -p $percent_writes -e $num_executors >> results.txt
            sleep 10
        done
    done
#done

