#!/bin/bash
#PMEM_CHUNK_SIZE=128
NKEYS=2000000
NTHREADS=10

echo "128B alignment running for basic config" >> results_wal_threads_128.txt

for ((thread = 1; thread <= NTHREADS; thread = thread + 1)); do
    echo "Cleaning up from last runs"
    rm -f /pmem/zfile
    sleep 10
    echo $thread >> results_wal_threads_128.txt
    ./src/nstore -a -e $thread -k $NKEYS >> results_wal_threads_128.txt
done

