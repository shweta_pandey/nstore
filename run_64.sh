#!/bin/bash
#PMEM_CHUNK_SIZE=64
NKEYS=2000000
NRUNS=5

echo "128B alignment running for basic config" >> results_wal.txt
echo "128B alignment running for basic config" >> results_lsm.txt
echo "128B alignment running for basic config" >> results_cow.txt

for ((run = 1; run <= NRUNS; run = run + 1)); do
    echo "Cleaning up from last runs"
    rm /dev/shm/zfile
    sleep 10
    ./src/nstore -k $NKEYS -a >> results/results_wal_64.txt
    sleep 10
    rm /dev/shm/zfile
    ./src/nstore -k $NKEYS -m >> results/results_lsm_64.txt
    sleep 10
    rm /dev/shm/zfile
    ./src/nstore -k $NKEYS -s >> results/results_cow_64.txt
    sleep 10
done
